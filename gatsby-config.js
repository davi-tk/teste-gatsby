module.exports = {
  pathPrefix: `/teste-gatsby`,
  plugins: [
    {
      resolve: "smooth-doc",
      options: {
        name: "Smooth DOC Starter",
        description: "Use your own description...",
        siteUrl: "https://example.com",

        sections: ['Inicio', 's2', 's3'],
      },
    },
  ],
};
